import smtplib
import boto3

# Import the email modules we'll need
from email.mime.text import MIMEText


def send_email(email_dict):#(from_add, to_add, email_content, subject, from_passwd):
    me = email_dict['from_add']
    you = email_dict['to_add']

    msg = MIMEText(email_dict['email_content'])

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = email_dict['subject']
    msg['From'] = me
    msg['To'] = you

    # Send the message via our own SMTP server, but don't include the
    # envelope header.

    s = smtplib.SMTP('smtp.gmail.com',587)
    s.ehlo()

    s.starttls()

    s_ssl = smtplib.SMTP_SSL('smtp.gmail.com',465)
    s_ssl.ehlo()

    s_ssl.login(me, email_dict['from_passwd'])

    header = 'Gmail Header'

    s_ssl.sendmail(me, [you], header+msg.as_string())

    s_ssl.quit()
    s.quit()
	

def lambda_handler(event, context):
    # TODO implement
    
    # Import smtplib for the actual sending function

	flag = 0 # There isn't wrong instance running
	num_instances = 0

	ec2 = boto3.resource('ec2')
	
	email_dict = {'from_add':'nextshift.whr11214@gmail.com', 
				  'to_add':'leslie@nextshifthealth.com', 
				  'email_content':'A non-t2.micro EC2 instance has been launched.', 
				  'subject':'a non-t2.micro EC2 instance has been launched', 
				  'from_passwd':'nextshiftwhr11214sui'}

	instances = ec2.instances.filter(
    	Filters=[{'Name': 'instance-state-name', 'Values': ['pending','running']}])
	
	print instances
	
	for item in instances:
		print item

	for instance in instances:
#		print(instance.id, instance.instance_type,instance.launch_time)
			
#		print '-----',instance.instance_type == 't2.micro' and datetime.datetime.now().toordinal() - instance.launch_time.toordinal() > 70,'-----'
#		print datetime.datetime.now(tzinfo=tzutc())
#		print instance.launch_time
#		print time.mktime(datetime.datetime.now(tzinfo=tzutc()).timetuple()) - time.mktime(instance.launch_time.timetuple())
		num_instances += 1
		if instance.instance_type != 't2.micro':# and datetime.datetime.now().toordinal() - instance.launch_time.toordinal() > 70:
			flag = 1
			email_dict['email_content'] += '\n Instance ID : ' + instance.id
			
		
	if flag != 0 and num_instances != 0:
		send_email(email_dict)

	print "end of the program......"
	print num_instances
	return 'Hello from Lambda'

if __name__ == '__main__':
	lambda_handler(1, 1)

